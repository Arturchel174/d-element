<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!--<pre>-->
<!--    --><?//var_dump($arResult)?>
<!--</pre>-->
<? if (!empty($arResult)): ?>
    <div class="inner-wrap">
        <div class="menu-block popup-wrap">
            <a href="" class="btn-menu btn-toggle"></a>
            <div class="menu popup-block">
                <ul class="">
                <?
                $previousLevel = 0;
                foreach ($arResult

                as $arItem): ?>

                <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
                    <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
                <? endif ?>


               <? if ($arItem["LINK"] != '/index.php') : ?>

                <? if ($arItem["IS_PARENT"]): ?>

                        <? if ($arItem["DEPTH_LEVEL"] == 1): ?>

                <li ><a href="<?= $arItem["LINK"] ?>" ><?= $arItem["TEXT"] ?></a>

                    <ul>
                        <div class="menu-text"><? $APPLICATION->ShowProperty("points")?> "<?=$arItem["TEXT"]?>"</div>
                        <? else: ?>
                        <li><a
                                    href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                            <ul>
                                <div class="menu-text"><? $APPLICATION->ShowProperty("points")?> "<?=$arItem["TEXT"]?>"</div>
                                <? endif ?>

                                <? else:?>

                                    <? if ($arItem["PERMISSION"] > "D"):?>

                                        <? if ($arItem["DEPTH_LEVEL"] == 1):?>

                                            <li ><a href="<?= $arItem["LINK"] ?>"
                                                  ><?= $arItem["TEXT"] ?></a>
                                            </li>
                                        <? else:?>
                                            <li ><a
                                                        href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>
                                        <? endif ?>

                                    <? else:?>

                                        <? if ($arItem["DEPTH_LEVEL"] == 1):?>
                                            <li><a href=""

                                                   title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                            </li>
                                        <? else:?>
                                            <li><a href=""
                                                   title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a>
                                            </li>
                                        <? endif ?>

                                    <? endif ?>

                                <? endif ?>
                                <?else:?>
                                    <li class="main-page"><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                                <? endif;?>


                                <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

                                <? endforeach ?>

                                <? if ($previousLevel > 1)://close last item tags?>
                                    <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
                                <? endif ?>
                            </ul>

                                <a href="" class="btn-close"></a>
            </div>
            <div class="menu-overlay"></div>
        </div>
    </div>
<? endif ?>