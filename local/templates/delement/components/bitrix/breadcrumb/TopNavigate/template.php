<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */


if (empty($arResult)) {
    return '';
}

$res = '<div class="breadcrumbs-box">
            <div class="inner-wrap">';

$elCount = count($arResult);
foreach ($arResult as $index => $item) {
    $link = (!empty($item['LINK']) && $index < ($elCount - 1)) ? $item['LINK'] : '#';
    $title = $item['TITLE'] ?? '';
    if($item == end($arResult)){
        $res .= '
                <span>' . $title . '</<span>
            ';
    }else {
        $res .= '
                <a href="' . $link . '">' . $title . '</a>
            ';
    }
}

$res .= '   
            </div>
        </div>';

return $res;
