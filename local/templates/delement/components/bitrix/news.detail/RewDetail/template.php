<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?php if (!empty($arResult['ID'])): ?>
    <hr>
    <header>
        <h1><?= $arResult["IBLOCK"]["NAME"].' - '. $arResult["NAME"]. ' - '. $arResult["PROPERTIES"]["COMPANY"]["VALUE"] ?></h1>
    </header>
    <div class="review-block">
        <div class="review-text">
            <div class="review-text-cont">
                <?= isset($arResult['DETAIL_TEXT']) ? $arResult['DETAIL_TEXT'] : ''; ?>
            </div>

            <div class="review-autor">

                <?= $arResult["NAME"] ?>,
                <?= $arResult["DISPLAY_ACTIVE_FROM"] ?>г,
                <?= $arResult["PROPERTIES"]["POSITION"]["VALUE"]?>,
                <?= $arResult["PROPERTIES"]["COMPANY"]["VALUE"]?>

            </div>
        </div>
        <div style="clear: both;" class="review-img-wrap">

            <img src="<?= isset($arResult['DETAIL_PICTURE']['SRC']) ? $arResult['DETAIL_PICTURE']['SRC']:
                SITE_TEMPLATE_PATH.'/assets/img/rew/no_photo.jpg' ?>"
                 alt="img">

        </div>
    </div>
    <div class="exam-review-doc">
        <p>Документы:</p>
        <? if(!empty(CFile::GetPath($arResult["PROPERTIES"]["DOCUMENT"]["VALUE"]))) : ?>

            <? foreach($arResult["PROPERTIES"]["DOCUMENT"]["VALUE"] as $var): ?>
                <? $name =  CFile::MakeFileArray($var)?>
                <div  class="exam-review-item-doc">
                    <img class="rew-doc-ico" src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icons/pdf_ico_40.png">
                    <a href="<?= CFile::GetPath($var);?>" download><?= $name["name"]?>></a></div>

            <? endforeach;?>
        <? else:?>
            <p>Не найдено</p>
        <? endif;?>
    </div>
    <hr>
<?php endif;?>